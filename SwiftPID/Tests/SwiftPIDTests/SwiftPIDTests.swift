import XCTest
@testable import SwiftPID


let kSampleTime = 0.01
let kSimulationTimeMax = 4.0

final class SwiftPIDTests: XCTestCase {
    let pid = PID(
        kp: 2.0,
        ki: 0.5,
        kd: 0.25,
        tau: 0.02,
        limitMin: -10,
        limitMax: 10,
        limitMinInt: -5,
        limitMaxInt: 5,
        t: kSampleTime
    )
    
    let pidController = PIDController()
    
    var output = 0.0
    let alpha = 0.02

    
    func testPID() {
        //PIDController_Init(&pid);
        pidController.reset(pid: pid)

        /* Simulate response using test system */
        let setPoint = 1.0

        print("t\tmeasurement\tpid.out")
        
        for t in stride(from: 0.0, to: kSimulationTimeMax, by: 0.01) {
            // Get measurement from system
            let measurement = update(previousOutput: pid.out)

            // Compute new control signal
            pidController.update(pid: pid, setpoint: setPoint, measurement: measurement)

            print("\(t)\t\(measurement)\t\(pid.out)")
        }
    }
    
    override func setUp() {
        self.output = 0.0
    }

    func update(previousOutput: Double) -> Double {
        self.output = (kSampleTime * previousOutput + output) / (1.0 + alpha * kSampleTime)
        return output;
    }
    
    
    func testExample() throws {
        // XCTest Documentation
        // https://developer.apple.com/documentation/xctest

        // Defining Test Cases and Test Methods
        // https://developer.apple.com/documentation/xctest/defining_test_cases_and_test_methods
    }
}
