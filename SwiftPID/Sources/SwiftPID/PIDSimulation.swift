//
//  PIDSimulation.swift
//  PIDControl
//
//  Created by Zakk Hoyt on 9/16/23.
//

import Foundation

//private let kSampleTime = 0.01
//private let kSimulationTimeMax = 10.0

import os

let logger = os.Logger(subsystem: "com.vaporwarewolf", category: "PIDSimulation")


@Observable public final class PIDSimulation {
    
    public struct Sample: Codable, Identifiable {
        public var id: Double { Date().timeIntervalSince1970 }
        public let t: Double
        public let setpoint: Double
        public let measurement: Double
        public let pidOutput: Double
    }
    
    public var pid: PID
    public let maxTime: TimeInterval

    public let pidController = PIDController()
    
    private var output = 0.0
    private let alpha = 0.02
    
    public var samples: [Sample] = [] {
        didSet {
            logger.debug("Did update samples: \(self.samples.count)")
            self.logLastSamples()
        }
    }
    
    public init(
        pid: PID,
        maxTime: TimeInterval
    ) {
        self.pid = pid
        self.maxTime = maxTime
        run()
    }
    
    public func run() {
        
//        logger.debug("\(self.pid.debugDescription)")
        // Reset output
        self.output = 0.0
        
        pidController.reset(pid: pid)

        /* Simulate response using test system */
        let setPoint = 1.0

        self.samples = stride(from: 0.0, to: maxTime, by: 0.01).map { t in
            // Get measurement from system
            let measurement = update(previousOutput: pid.out)

            // Compute new control signal
            pidController.update(pid: pid, setpoint: setPoint, measurement: measurement)

            return Sample(t: t, setpoint: setPoint, measurement: measurement, pidOutput: pid.out)
        }
    }
    
    private func update(previousOutput: Double) -> Double {
        self.output = (pid.t * previousOutput + output) / (1.0 + alpha * pid.t)
        return output;
    }
    
    private func logLastSamples(count: Int = 5) {
        logger.debug("Computed \(self.samples.count) samples")
        logger.debug("\("t", align: .left(columns: 10)) \("measurement", align: .left(columns: 20)) \("pid.out", align: .left(columns: 20))")
        //for sample in samples {
        for i in (samples.count - count)..<(samples.count) {
            let sample = samples[i]
            logger.debug("\(sample.t, align: .left(columns: 10)) \(sample.measurement, align: .left(columns: 20)) \(sample.pidOutput, align: .left(columns: 20))")
        }
    }
}
