import SwiftUI
import Foundation

public protocol PIDModel {
    
}

/// contains gains, stroge vars
/// https://www.youtube.com/watch?v=zOByx3Izf5U
@Observable public class PID: PIDModel {
    
    // Controller gains
    public var kp: Double = 0
    public var ki: Double = 0
    public var kd: Double = 0

    // Derivative low-pass filter time constant
    public var tau: Double = 0

    // Output limits
    public var limitMin: Double = 0
    public var limitMax: Double = 0

    // Integrator limits
    public var limitMinInt: Double = 0
    public var limitMaxInt: Double = 0

    #warning("FIXME: @zakkhoyt - Shouldn't this be passed on each update?")
    // Sample time (in seconds)
    public var t: Double = 0

    // Controller "memory"
    public internal(set) var integrator: Double = 0
    public internal(set) var prevError: Double = 0
    public internal(set) var differentiator: Double = 0 // Required for integrator
    public internal(set) var prevMeasurement: Double = 0 // Required for differentiator

    // Controller output
    public internal(set) var out: Double = 0
    
    public init(
        kp: Double,
        ki: Double,
        kd: Double,
        tau: Double = 0,
        limitMin: Double,
        limitMax: Double,
        limitMinInt: Double,
        limitMaxInt: Double,
        t: Double = 0
    ) {
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.tau = tau
        self.limitMin = limitMin
        self.limitMax = limitMax
        self.limitMinInt = limitMinInt
        self.limitMaxInt = limitMaxInt
        self.t = t
        self.integrator = 0
        self.prevError = 0
        self.differentiator = 0
        self.prevMeasurement = 0
        self.out = 0
    }
}


extension PID: CustomDebugStringConvertible {
    public var debugDescription: String {
        "pid values:\n" + Mirror.reflectProperties(of: self, matchingType: Double.self).map { tuple in
            "\t\(tuple.name): \(tuple.value)"
        }.joined(separator: "\n")
    }
}
