// The Swift Programming Language
// https://docs.swift.org/swift-book


import SwiftUI
import Foundation

public protocol PIDControllable {
    func reset(
        pid: PID
    )
    
    func update(
        pid: PID,
        setpoint: Double,
        measurement: Double
    ) -> Double
}

public class PIDController: PIDControllable {
    public init() {}

    public func reset(
        pid: PID
    ) {

        /* Clear controller variables */
        pid.integrator = 0.0
        pid.prevError  = 0.0

        pid.differentiator  = 0.0
        pid.prevMeasurement = 0.0

        pid.out = 0.0
    }

    @discardableResult
    public func update(
        pid: PID,
        setpoint: Double,
        measurement: Double
    ) -> Double {
        // https://youtu.be/zOByx3Izf5U?t=780
        

        // Error signal
        let error = setpoint - measurement

        // Proportional
        let proportional = pid.kp * error

        // Integral
        pid.integrator = pid.integrator + 0.5 * pid.ki * pid.t * (error + pid.prevError)

        // Anti-wind-up via integrator clamping
        pid.integrator = max(min(pid.integrator, pid.limitMaxInt), pid.limitMinInt)

        // Derivative (band-limited differentiator)
        pid.differentiator = -(2.0 * pid.kd * (measurement - pid.prevMeasurement) // Note: derivative on measurement, not error. therefore minus sign in front of equation!
                            + (2.0 * pid.tau - pid.t) * pid.differentiator)
                            / (2.0 * pid.tau + pid.t)
                
        
//        pid.differentiator = (2.0 * pid.kd * (error - pid.prevError)    /* Note: derivative on measurement, therefore minus sign in front of equation! */
//                             + (2.0 * pid.tau - pid.t) * pid.differentiator)
//                             / (2.0 * pid.tau + pid.t)

//        pid.differentiator = pid.kd * (error - pid.prevError)


        // Compute output and apply limits
        pid.out = proportional + pid.integrator + pid.differentiator

        pid.out = max(min(pid.out, pid.limitMax), pid.limitMin)
        
        // Store error and measurement for later use
        pid.prevError = error
        pid.prevMeasurement = measurement

        // Return controller output
        return pid.out
    }
}

 

extension Mirror {
    /// Inspects the properties of the instance of `target` to distill a list of all properties of `matchingType`
    /// - Parameters:
    ///   - target: The instance to inspect/reflect.
    ///   - type: The type of interest
    /// - Returns: A list of tuples `(propertyName: String, property: T)`
    /// - Remark: This does not apply to computed properties.
    public static func reflectProperties<T>(
        of target: Any,
        matchingType type: T.Type = T.self
    ) -> [(name: String, value: T)] {
        Mirror(reflecting: target)
            .children
            .compactMap {
                guard let label = $0.label, let value = $0.value as? T else {
                    return nil
                }
                return (label, value)
            }
    }
}

