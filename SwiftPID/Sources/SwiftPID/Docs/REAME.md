



# References
* brian douglas
  * [PID Control - A brief introduction](https://www.youtube.com/watch?v=UR0hOmjaHp0)
  * [Simple Examples of PID Control]([https://www.youtube.com/watch?v=XfAt6hNV8XM)
* Scott Hayes
  * [PID Math Demystified](https://www.youtube.com/watch?v=JEpWlTl95Tw)
    * A good visual sketch video 
* Johannes Spielmann
  * [Controlling a spaceship](https://gitlab.com/jspielmann/shippid/)
* Software struct
  * [here](https://youtu.be/zOByx3Izf5U?t=661)



# Terms

* Plant (the object ot control)
* P (proportion)
* I (intregral)
* D (derivative)
* set point (ideal position)
* processValue / value / actual (measured position)
* error (ideal - measured)
* 

--- 

# C Example with P, PI, PD, PID
* [link](https://www.cytron.io/tutorial/pid-for-embedded-design)
* [see local copy](/Users/zakkhoyt/code/repositories/github/pid/c/Web/PID for Embedded Design.html)

```c
// Infinite loop.
while (1)
{
  // Get the current position.
  current_position - read_current_position();

  // Calculate the error.
  error = target_position - current_position;

  // Calculate the integral.
  integral = integral + error:

  // Calculate the derivative.
  derivative = error - last_error:

  // Calculate the Control Variable.
  pwm = (kp * error) + (ki * integral) + (kd * derivative);

  // Limit the Control Variable to within +-255.
  if (pvm > 255) {
    pvm = 255;
  } else if (pvm < -255) {
    pvm = -255;
  }

  
  if (pwm > 0) {
    // If the Control Variable is positive, run the motor clockwise.
    motor_cv(pwm);
  } else if (pwm < 0) {
    // If the Control Variable is negative, run the motor counter clockwise.
    motor_ccv(-pwm);
  } else {
    // If the Control Variable is zero, stop the motor.
    motor_stop();
  }

  // Save the current error as last error for next iteration.
  last_error = error;
}
```


# C Example (SO)
* [link](https://softwareengineering.stackexchange.com/questions/186124/programming-pid-loops-in-c)
  
Before getting your feet wet in C (sea?)

I assume you are reading the signals from some kind of analogue to digital converter. If not then you would have to simulate the signal as an input.

If using Standard form we have,

Assuming the the loop running time is small enough (a slow process), we can use the following function for calculating output,

```
output = Kp * err + (Ki * int * dt) + (Kd * der /dt);
```
where:
Kp = Proptional Constant.
Ki = Integral Constant.
Kd = Derivative Constant.
err = Expected Output - Actual Output ie. error;
int  = int from previous loop + err; ( i.e. integral error )
der  = err - err from previous loop; ( i.e. differential error)
dt = execution time of loop.


where initially 'der' and 'int' would be zero. If you use a delay function in code to tune the loop frequency to say 1 KHz then your dt would be 0.001 seconds.


# C Educational Article
* [link](https://www.scilab.org/discrete-time-pid-controller-implementation)
* [See local copy](/Users/zakkhoyt/code/repositories/github/pid/c/Web/Discrete-time PID Controller Implementation Scilab.html)

```c
// global variables

// variables used in PID computation
double e2, e1, e0, u2, u1, u0; 

// command
double r; 

// plant output
double y; 

// -- these parameters should be user-adjustable 

double Kp = 10; // proportional gain

double Ki = 1; // integral gain

double Kd = 1; // derivative gain

double N = 20; // filter coefficients

double Ts = 0.01; // This must match actual sampling time PID

// controller is running

// -- the following coefficients must be recomputed if
// -- any parameter above is changed by user

a0 = (1 + N * Ts);

a1 = -(2 + N*Ts);

a2 = 1;

b0 = Kp * (1 + N * Ts) + Ki * Ts * (1 + N * Ts) + Kd * N;

b1 = -(Kp * (2 + N * Ts) + Ki * Ts + 2 * Kd * N);

b2 = Kp + Kd * N;

ku1 = a1 / a0; 
ku2 = a2 / a0; 
ke0 = b0 / a0; 
ke1 = b1 / a0; 
ke2 = b2 / a0;

// implement this as timer interrupt service routine 
void pid(void) {
  // update variables
  e2 = e1; 
  e1 = e0; 
  u2 = u1;
  u1 = u0; 

  // read plant output
  y = read();

  // compute new error
  e0 = r – y;

  // eq (12)
  u0 = -ku1 * u1 – ku2 * u2 + ke0 * e0 + ke1 * e1 + ke2 * e2; 

  // limit to DAC or PWM range
  if (u0 > UMAX) u0 = UMAX;  
  if (U0 < UMIN) u0 = UMIN;

  // sent to output
  write(u0);
}
```