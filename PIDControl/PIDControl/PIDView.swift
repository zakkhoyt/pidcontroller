//
//  ContentView.swift
//  PIDControl
//
//  Created by Zakk Hoyt on 9/16/23.
//

import Charts
import SwiftUI
import SwiftPID

struct PIDView: View {
    var simulation: PIDSimulation
    
    var body: some View {
        VStack {
            
            Chart {
                ForEach(simulation.samples) { sample in
                    LineMark(
                        x: .value("t", sample.t),
                        y: .value("setPoint", sample.setpoint)
                    )
                    .foregroundStyle(by: .value("setPoint", "setPoint"))
                }
                                
                ForEach(simulation.samples) { sample in
                    LineMark(
                        x: .value("t", sample.t),
                        y: .value("measurement", sample.measurement)
                    )
                    .foregroundStyle(by: .value("measurement", "measurement"))
                }
            }
            
            Chart {
                ForEach(simulation.samples) { sample in
                    LineMark(
                        x: .value("t", sample.t),
                        y: .value("setPoint", sample.setpoint)
                    )
                    .foregroundStyle(by: .value("setPoint", "setPoint"))
                }
                
                
                ForEach(simulation.samples) { sample in
                    LineMark(
                        x: .value("t", sample.t),
                        y: .value("pid.out", sample.pidOutput)
                    )
                    .foregroundStyle(by: .value("pid.out", "pid.out"))
                }
                
                ForEach(simulation.samples) { sample in
                    LineMark(
                        x: .value("t", sample.t),
                        y: .value("measurement", sample.measurement)
                    )
                    .foregroundStyle(by: .value("measurement", "measurement"))
                }
            }
            
            Text("# samples: \(simulation.samples.count)")
            Text("kp: \(simulation.pid.kp)")
            Text("ki: \(simulation.pid.ki)")
            Text("kd: \(simulation.pid.kd)")

            Slider(
                value: Binding<Double>(
                    get: {
                        simulation.pid.kp
                    },
                    set: {
                        simulation.pid.kp = $0
                        simulation.run()
                    }
                ),
                in: 0.0...10.0
            ) {
                Text("p gain")
            }
            
            Slider(
                value: Binding<Double>(
                    get: {
                        simulation.pid.ki
                    },
                    set: {
                        simulation.pid.ki = $0
                        simulation.run()
                    }
                ),
                in: 0.0...10.0
            ) {
                Text("i gain")
            }
            
            Slider(
                value: Binding<Double>(
                    get: {
                        simulation.pid.kd
                    },
                    set: {
                        simulation.pid.kd = $0
                        simulation.run()
                    }
                ),
                in: -4.0...10.0
            ) {
                Text("d gain")
            }
            
            Slider(
                value: Binding<Double>(
                    get: {
                        simulation.pid.tau
                    },
                    set: {
                        simulation.pid.tau = $0
                        simulation.run()
                    }
                ),
                in: 0.0 ... 0.5,
                step: 0.01
            ) {
                Text("tau")
            }

        }
        .padding()
    }
}

#Preview {
    PIDView(
        simulation: PIDSimulation.impl0
    )
}


