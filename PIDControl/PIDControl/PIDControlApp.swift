//
//  PIDControlApp.swift
//  PIDControl
//
//  Created by Zakk Hoyt on 9/16/23.
//

import SwiftPID
import SwiftUI
import os

let logger = os.Logger(
    subsystem: "com.vaporwarewolf",
    category: "PIDSimulation"
)

let kSampleTime = 0.01
let kSimulationTimeMax = 10.0

@main
struct PIDControlApp: App {
    let simulation = PIDSimulation.impl0
    
    init() {}
    
    var body: some Scene {
        WindowGroup {
            PIDView(simulation: simulation)
        }
    }
}

extension PIDSimulation {
    static let impl0 = PIDSimulation(
        pid: PID(
            kp: 0.4,
            ki: 0.2,
            kd: 0.25,
            tau: 0.01,
            limitMin: -10,
            limitMax: 10,
            limitMinInt: -5,
            limitMaxInt: 5,
            t: kSampleTime
        ),
        maxTime: kSimulationTimeMax
    )
}
