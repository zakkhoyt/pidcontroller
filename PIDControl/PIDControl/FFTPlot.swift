//
//  LinePlot.swift
//  Chart
//
//  Created by Zakk Hoyt on 7/25/23.
//

//import SwiftUI
//import Charts
//
//class FFTPlotViewModel {
//    let title: String
//    let samples: [FFTSample]
//    
//    init(
//        title: String = "live capture",
//        samples: [FFTSample]
//    ) {
//        self.title = title
//        self.samples = samples
//    }
//    
//    init(jsonFile: String) {
//        self.title = jsonFile
//        guard let url = Bundle.main.url(forResource: jsonFile, withExtension: "json") else {
//            fatalError("Could not find file: \(jsonFile)")
//        }
//        do {
//            let data = try Data(contentsOf: url)
//            let decoder = JSONDecoder()
//            let samples: [FFTSample] = try decoder.decode([FFTSample].self, from: data)
//            self.samples = samples
//        } catch {
//            fatalError(error.localizedDescription)
//        }
//    }
//}
//
//public struct FFTPlotView: View {
//    public init(audioBufferProvider: AudioBufferProvider) {
//        self.audioBufferProvider = audioBufferProvider
//    }
//    
//    var audioBufferProvider: AudioBufferProvider
//    
//    public var body: some View {
//        FFTPlot(samples: audioBufferProvider.fftRawSamples)
//    }
//}
//
//struct FFTPlot: View {
//    var samples: [FFTSample]
//    
//    enum XZoomLevel: Int, CaseIterable, Identifiable, CustomStringConvertible {
//        case max
//        case medium
//        case min
//        case normal
//        
//        var id: Int {
//            rawValue
//        }
//
//        var doubleValue: Double {
//            switch self {
//            case .max: 300
//            case .medium: 500
//            case .min: 1000
//            case .normal: 22050
//            }
//        }
//
//        var description: String {
//            "0-\(String(format: "%.1f", doubleValue)) Hz"
//        }
//    }
//    
//    @State private var frequency: Float?
//    @State private var xDomainMax: Double = 1000
//    @State var xZoomSelection: Int = XZoomLevel.medium.rawValue
//    
//    var body: some View {
//        VStack {
//            Text("FFT Plot")
//            
//            Chart {
//                ForEach(samples) { sample in
//                    LineMark(
//                        x: .value("fMid", sample.frequencyMiddle),
//                        y: .value("fft imaginary", sample.imaginary)
//                    )
//                    .foregroundStyle(by: .value("Imaginary", "Imaginary"))
//                    .interpolationMethod(.catmullRom)
//                    .symbol() {
//                        if abs(sample.imaginary) > 10 {
//                            ZStack {
//                                Text("\(String(format: "%.1f", sample.frequencyMiddle)) Hz")
//                                    .offset(x: 0, y: sample.imaginary < 0 ? 12 : -12)
//                                    .foregroundColor(.red)
//                                    .font(.caption2)
//                                Circle()
//                                    .fill(.red)
//                                    .frame(width: 8, height: 8)
//                                
//                            }
//                        }
//                    }
//                    .symbolSize(30)
//                }
//                
//                ForEach(samples) { sample in
//                    LineMark(
//                        x: .value("fMid", sample.frequencyMiddle),
//                        y: .value("fft real", sample.real)
//                    )
//                    .foregroundStyle(by: .value("Real", "Real"))
//                    .interpolationMethod(.catmullRom)
//                    .symbol() {
//                        if abs(sample.real) > 10 {
//                            ZStack {
//                                Text("\(String(format: "%.1f", sample.frequencyMiddle)) Hz")
//                                    .offset(x: 0, y: sample.real < 0 ? 12 : -12)
//                                    .foregroundColor(.blue)
//                                    .font(.caption2)
//                                Circle()
//                                    .fill(.blue)
//                                    .frame(width: 8, height: 8)
//                                
//                            }
//                        }
//                    }
//                    .symbolSize(30)
//                }
//                
//                if let frequency {
//                    RectangleMark(
//                        x: .value("Frequency", frequency)
//                    )
//                    .foregroundStyle(.primary.opacity(0.2))
//                    .annotation(
//                        position: .trailing,
//                        alignment: .topTrailing,
//                        spacing: 8
//                    ) {
//                        Text("\(String(format: "%.1f", frequency)) Hz")
//                    }
//                }
//            }
//            //.chartXScale(domain: 0.0 ... xDomainMax, type: .linear)
//            .chartXScale(
//                domain: 0.0 ... {
//                    guard let xZoomLevel = XZoomLevel(rawValue: xZoomSelection) else { return xDomainMax }
//                    return xZoomLevel.doubleValue
//                }(),
//                type: .linear
//            )
//            .chartForegroundStyleScale([
//                "Imaginary": .red, "Real": .cyan
//            ])
//            //        .chartOverlay { (chartProxy: ChartProxy) in
//            //            Color.clear
//            //                .onContinuousHover { (hoverPhase: HoverPhase) in
//            //                    switch hoverPhase {
//            //                    case .active(let hoverLocation):
//            ////                        selectedMonth = chartProxy.value(
//            ////                            atX: hoverLocation.x, as: String.self
//            ////                        )
//            //                        let frequency = chartProxy.value(atX: hoverLocation.x, as: Float.self)
//            //                        self.frequency = frequency
//            //                    case .ended:
//            //                        self.frequency = nil
//            //                    }
//            //                }
//            //        }
//            
//            Picker(
//                selection: $xZoomSelection,
//                label: Text("xZoom"),
//                content: {
//                    ForEach(XZoomLevel.allCases) { z in
//                        Text(z.description)
//                    }
//                }
//            )
//            
//            HStack {
//                Button {
//                    FFTSample.save(samples: samples)
//                } label: {
//                    Text("Save")
//                }
//            }
//        }
//        .padding()
//    }
//}
//
//#Preview {
//    FFTPlot(
//        samples: FFTPlotViewModel(jsonFile: "fft_512").samples
//    )
//}
//
